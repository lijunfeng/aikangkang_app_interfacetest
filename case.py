#-*- coding:utf-8 -*-
from aikangkang_app_interface_test import Interface_TEST

import unittest,os
#OK_为不可重复测试case，每次测试需要编辑

class InterfaceTest(unittest.TestCase,Interface_TEST):
	'''
	'''
	def test_login_case1(self):
		'''正确的用户名密码
		'''
		r = self.log_in(u'18511567123',u'123456')
		self.assertTrue(r)

	def test_login_case2(self):
		'''错误的用户名或密码
		'''
		r = self.log_in(u'18511567123',u'1234567')
		self.assertFalse(r)

		
	def OK_test_signup_case1(self): #注册账号 手机好为13717600002 密码为123456 ——已使用
		'''#合法的用户名、密码
		#注意每次执行前需要更换参数，否则会Fail
		'''
		r = self.sign_up(u'13717600002',u'接口测试02',u'123456')
		self.assertTrue(r)
		

	def test_signup_case2(self):
		'''已注册的用户名、密码
		#注意：该参数为已经注册过的即可
		'''
		r = self.sign_up(u'13717600001',u'接口测试3',u'123456')
		self.assertFalse(r)

	def test_signup_case3(self):
		'''不合法的用户名或密码
		'''
		r = self.sign_up(u'13717607896123',u'接口测试4',u'123456')
		self.assertFalse(r)
	
	def test_home_image_case(self):
		'''获取首页图片
		'''
		r = self.home_page_advertising_image()
		print u'\nthe home image are',r

	def test_coach_list_case(self):
		'''显示教练列表
		'''
		r = self.coach_list()
		print '\nthe list of coaches is ',r

	def test_get_user_info_case1(self):
		'''获取一个存在的用户信息
		'''
		r =self.get_user_info(265)
		print '\nthe mobile of the user is %s \n' %r

	def test_get_user_info_case2(self):
		'''获取一个不存在的用户信息
		'''
		r =self.get_user_info(2739400)
		self.assertEqual(r,u'') # 注意此处返回的是个空 但是在此处不知为什么是False

	def OK_test_change_phone_case1(self):
		'''修改绑定手机——正确
		'''
		r = self.change_phone(13,u'13717607755',u'123456',u'13717607701')  #将7755的手机好换成7701  ——已使用
		self.assertTrue(r) #判断真假
		r2 = self.log_in(u'13717607701',u'123456') #登录测试
		self.assertTrue(r2) 		
		
	def test_change_phone_case2(self): # 使用注册过的手机号 作为旧密码-13717600002,314(uid) pwd123456
		'''修改绑定手机——密码错误
		'''
		r= self.change_phone(314,13717600002,u'123456789',13717600003) #0002换成0003失败，密码错误
		self.assertFalse(r)
	def test_change_phone_case3(self):
		'''修改绑定手机——新手机号已经注册
		'''
		r= self.change_phone(314,13717600002,u'123456789',13717600001) #0002换成0001失败，0001手机已经注册过了
		self.assertFalse(r)

	def test_change_phone_case3(self):
		'''修改绑定手机——旧手机号错误
		'''
		r= self.change_phone(314,13717600010,u'123456789',13717600011) #0010换成0011失败，0010手机未注册过
		self.assertFalse(r)

	def test_collect_course_show_case1(self):
		'''显示265用户的收藏——从第1个开始显示，显示10条
		'''
		r =self.collect_course_show(265,1,10)
		print 'the list of collection is ',r
		self.assertTrue(len(r)>0)

	def test_collect_course_show_case2(self):
		'''显示某用户的收藏——第2个开始显示，显示3条 #前提是有第二个收藏
		'''
		r =self.collect_course_show(265,2,3)
		print 'the list of collection is ',r
		self.assertTrue(len(r)==3) #确定该用户有4条以上收藏

	def test_collect_add_case(self):#这条case 和下一条成对儿出现
		'''265用户添加收藏96
		'''
		r = self.collect_add(265,96) #添加收藏，该课程没有在收藏列表中，成功收藏
		self.assertTrue(r)
		r = self.collect_add(265,96) #添加收藏，该课程已经在收藏列表中，失败
		self.assertFalse(r)

	def test_collect_del_case(self): #这条case 和上一条成对儿出现
		'''某用户删除收藏
		'''
		r = self.collect_del(265,96) #删除收藏，该课程在收藏列表中，成功删除
		self.assertTrue(r)
		r = self.collect_del(265,96) #删除收藏，该课程不在收藏列表中，失败
		self.assertFalse(r)

	def test_order_Info_case(self):
		'''查看某订单详细信息 #优化需要配合数据mysqldb测试
		'''
		r = self.order_info(u'C2015041414242256173') #存在的订单 成功获取订单title
		print 'C2015041414242256173 订单的 title is ',r.encode('utf-8')
		self.assertTrue(len(r)>0)
		r = self.order_info(u'C2015041414242256173234') # 不存在订单，失败
		self.assertFalse(r)

	def test_order_List_case(self):
		'''该用户的订单list显示 #优化需要配合数据mysqldb测试
		'''
		r  = self.order_list(265,0,5)
		self.assertTrue(r)

	def test_head_portrait_change_case(self):
		'''给265用户上传新图片
		'''
		path = os.path.join(os.path.dirname(__file__),'111.jpg')  #将头像文件和脚本文件放在一个目录 111.jpg  222.jpg
		r = self.head_portrait_change(265,path,16)
		self.assertTrue(r)

	def test_modify_password_case1(self):
		'''修改密码——旧密码错误
		'''
		r = self.modify_password('13717600001','12345','123456')  #新旧密码可以相同
		self.assertFalse(r)


	def test_modify_password_case2(self):
		'''修改密码——新密码格式错误
		'''
		r = self.modify_password('13717600001','123456','')  #密码不能为空
		self.assertFalse(r)

	def test_modify_password_case3(self):
		'''修改密码——正确密码
		'''
		r = self.modify_password('13717600001','123456','123456')  #新旧密码可以相同
		self.assertTrue(r)

	def test_find_back_password_case1(self):
		'''找回密码功能——正确
		'''
		r = self.find_back_password('13717600001',313,u'123456') #将0001 的密码设置为123456
		self.assertTrue(r)

	def test_find_back_password_case2(self):
		'''找回密码功能——不存在手机
		'''
		r = self.find_back_password('13717600021',313,u'123456') #将0021 的密码设置为123456，失败设置 手机没有注册
		self.assertFalse(r)

	def test_find_back_password_case3(self):
		'''找回密码功能——新密码格式不正确时
		'''
		r = self.find_back_password('13717600001',313,u'') #将0001 的密码设置为空 失败设置
		self.assertFalse(r)


	def test_modiy_user_info_case1(self):
		'''修改个人信息——男 1988-05-25
		'''
		r = self.modiy_user_info(265,u'1988-05-25',u'male')
		self.assertTrue(r)

	def test_modiy_user_info_case2(self):
		'''修改个人信息——女 1988-05-25
		'''
		r = self.modiy_user_info(265,u'1988-05-25',u'female')
		self.assertTrue(r)

	def test_modiy_user_info_case3(self):
		'''修改个人信息——保密 1988-05-25
		'''
		r = self.modiy_user_info(265,u'1988-05-25',u'secret')
		self.assertTrue(r)

	def test_modiy_user_info_case4(self):
		'''修改个人信息——保密 2017-05-25 ——不能通过
		'''
		#r = self.modiy_user_info(265,u'2017-05-25',u'female')  # bug  未来日期不允许通过
		#self.assertFalse(r)
		pass

	def test_send_verification_case(self):
		'''向某手机发送验证码
		'''
		r = self.send_verification(18511567123,265)
		print 'this verify code is ',r
		self.assertTrue(len(str(r))>1)

	def test_get_all_course_case(self):
		'''获取所有课程类目
		'''
		r = self.get_all_course()
		print u'the list of classes is ',r
		self.assertTrue(len(r)>0)

	def test_get_all_course_by_fclass_case(self):
		'''获取所有类目，每个类目下带4个课程
		'''
		r = self.get_all_course_by_fclass()
		print u'the list of classes is ',r
		self.assertTrue(len(r)>0)

	def test_get_course_by_page_num_case1(self):
		'''获取所有课程——显示第1个，且显示1个
		'''
		r = self.get_course_by_page_num(0,1)
		self.assertTrue(r)


	def test_get_course_by_page_num_case2(self):
		'''获取所有课程——显示第2条 && 第8页
		'''
		r = self.get_course_by_page_num(1,8)
		self.assertTrue(r)
#----------------------
#有氧健身 id =2
#减肥塑形    3
#民俗健身   4
#东方养生    5
#功夫武术   6
#世界舞蹈   8
#男子健身   11
#身体康复   12
	def test_get_class_of_all_courses_case1(self):
		'''获取 有氧健身 类目的全部课程共8个类目
		'''
		r = self.get_class_of_all_courses(2,1)
		print r
		self.assertTrue(len(r)>0)
	def test_get_class_of_all_courses_case2(self):
		'''获取 减肥塑形 类目的全部课程共8个类目
		'''
		r = self.get_class_of_all_courses(3,1)
		print r
		self.assertTrue(len(r)>0)
	def test_get_class_of_all_courses_case3(self):
		'''获取 民俗健身 类目的全部课程共8个类目
		'''
		r = self.get_class_of_all_courses(4,1)
		print r
		self.assertTrue(len(r)>0)
	def test_get_class_of_all_courses_case4(self):
		'''获取 东方养生 类目的全部课程共8个类目
		'''
		r = self.get_class_of_all_courses(5,1)
		print r
		self.assertTrue(len(r)>0)
	def test_get_class_of_all_courses_case5(self):
		'''获取 功夫武术 类目的全部课程共8个类目
		'''
		r = self.get_class_of_all_courses(6,1)
		print r
		self.assertTrue(len(r)>0)
	def test_get_class_of_all_courses_case6(self):
		'''获取 世界舞蹈 类目的全部课程共8个类目
		'''
		r = self.get_class_of_all_courses(8,1)
		print r
		self.assertTrue(len(r)>0)
	def test_get_class_of_all_courses_case7(self):
		'''获取 男子健身 类目的全部课程共8个类目
		'''
		r = self.get_class_of_all_courses(11,1)
		print r
		self.assertTrue(len(r)>0)

	def test_get_class_of_all_courses_case8(self):
		'''获取 身体康复 类目的全部课程共8个类目
		'''
		r = self.get_class_of_all_courses(12,1)
		print r
		self.assertTrue(len(r)>0)
#-----------------------
	def test_get_info_course_case(self):
		'''获取某课程的详细信息
		'''
		r= self.get_info_course(96,265) 
		print r
		self.assertTrue(len(r)>0)

	def test_get_class_list_case(self):
		'''获取某个课程的课时列表
		'''
		r = self.get_class_list(96,1)
		self.assertTrue(len(r))

	def test_get_comment_case(self):
		'''获取某个课程的评论
		'''
		r =self.get_comment(273,0)
		self.assertTrue(len(r)>0)


	def test_add_comment_case(self):
		'''给某课程添加评论——有中文、英文和标点
		'''
		r = self.add_comment(265,273,u'自动化测试','5')
		self.assertTrue(u'自动化测试'==r)

	def OK_test_add_course_case(self):  # 注意 每次运行需要修改 使用新的课程ID，且该id为添加在我的学习列表中
		'''将某课程添加到课程列表
		'''
		r =self.add_course(23,265) # 添加到学习列表_之前未添加过
		self.assertTrue(r)
		r =self.add_course(23,265) # 添加到学习列表 ————已经添加过了，不能再添加
		self.assertFalse(r)		

	def test_get_courses_list_case(self):
		'''获取个人课程列表内容
		'''
		r = self.get_courses_list(265,3,0) # 获取3个课程，第一页的
		self.assertTrue(len(r)>0)

		r = self.get_courses_list(26511,6,0) # 一个不存在的userid获取失败
		self.assertFalse(r)

	def test_filter_list_case(self):
		'''搜索——中文、英文、空
		'''
		r = self.filter_list(u'app',0) #搜索"app"
		self.assertTrue(len(r)>0)
		r =self.filter_list(u'加拉加斯',0) #搜索一个不存在的课程，失败返回False
		self.assertFalse(r)

	def test_get_collect_cousrses_order_case(self):
		'''获取某用户的收藏课程数、课程数、订单数（课程数==订单数）
		'''
		r = self.get_collect_cousrses_order(265)
		print u'订单数量'.encode('utf-8'),r[u'order_num'],u'\n收藏数： '.encode('utf-8'),r[u'stow_count'],u'\n学习列表课程： '.encode('utf-8'),r[u'learn_courses_count']
	
	




if __name__=='__main__':
	suite =unittest.TestLoader().loadTestsFromTestCase(InterfaceTest)
	unittest.TextTestRunner(verbosity=2).run(suite)