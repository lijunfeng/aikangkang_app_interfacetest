#-*- coding:utf-8 -*-
import urllib,urllib2,urlparse,requests

import json,os,time,re
import MySQLdb

#urlparse.urlparse(url) 将url拆分成
#urlparse.urlunparse() 将拆分的url部分整合
#urlparse.urljoin() 将两个url部分 用/连接起来
#urllib.encode(dict)  将字典格式翻译成url的格式
#interface test     http://akkedu.hbl365.com/apitest/list

#优化和问题
#1 优化 : 目前只判断真假，并没有将测试结果与数据库比对
#2 优化 ：首页轮播图需要使用re模块，找到关键词语case中的image name做比对
#3 问题 ：涉及到验证码问题（修改密码 修改绑定手机）时，可以去数据库查看验证码，返回该验证码

#test environment
URL = u'http://akkedu.hbl365.com'

# 
#URL = 

class Interface_TEST(object):
      '''
      the base of class of interface test.
      '''


      def sign_up(self,username,nname,pwd):  
            '''
            注册接口.
            Methodget.
            there are three parameter. contain  'mobile','nickname' and 'password'
            '''

            #get the information of the user who had signed up 
            reg_url =urlparse.urljoin(URL,'mapi_v1/user_register')
            data = {
                  'mobile':username,
                  'nickname':nname,
                  'password':pwd
                  }
            Result =requests.get(reg_url,params=data).json()

            #if the status of return parameter is 0, it can not sign up
            #if the status of return parameter is 1, it had signed up
            if Result['status'] is 1:
                  return True
            else:
                  return False

      def log_in(self,username,passwd):
            '''
            登录.
            Method : get
            there are 2 parameter. contain : 'mobile','_password'
            '''

            login_url = urlparse.urljoin(URL,r'mapi_v1/login') 

            data = {
                  u'mobile':username,
                  u'_password':passwd
                  }
            data_req = urllib.urlencode(data)
            #encode the get url (the url and parameters join via '?')
            login_url =login_url+'?'+data_req

            respone = urllib2.urlopen(login_url)
            result = respone.read()
            Result =json.loads(result)


            if Result['status'] == True:
                  return True
            else:
                  return False

      def home_page_advertising_image(self):
            '''
            首页录播图
            get
            no parameter
            '''

            #get the image information via url
            advertising_image_url = urlparse.urljoin(URL, r'mapi_v1/get/banner')
            result = urllib2.urlopen(advertising_image_url).read()
            Res = json.loads(result)
            #print Res
            if len(Res)==0:
                  return False
            if len(Res)>=1:
                  image_list=[]
                  for i in Res:
                        pm =re.search(u'mobile.*',i[u'src'])
                        image_list.append(pm.group())
            return image_list
            '''
            #可以使用文件方式
            #save the image in file(adv_image.txt)
            filename = os.path.join(os.path.dirname(__file__),'adv_image.txt')
            f = file(filename,'w+')

            for i in Res:
                  for j in i.keys():
                        f.write(j)
                        f.write(' : ')
                        f.write(i[j])
                        f.write('\n')
                  f.write('\n')
            f.close()
            '''
      
      def coach_list(self):
            '''首页教练接口
            method get
            parameters none
            '''
            get_coaches_url =urlparse.urljoin(URL,'mapi_v1/coach/list')
            Res = requests.get(get_coaches_url).json()
            #print Res
            coachs = []
            for i in Res[u'list']:
                  coachs.append(i[u'nickname'])
            return coachs

      def get_user_info(self,uid):
            '''
            个人基本信息获取接口
            Method get
            parameter is userid

            '''
            get_url = urlparse.urljoin(URL, r'mapi_v1/user/info')
            data = {
            'userid':uid
            }
            data = urllib.urlencode(data)
            get_url = get_url+'?'+data
            res = urllib2.urlopen(get_url).read()
            Res = json.loads(res)
            #if Res['status'] is True:  注意 is 不能乱用 1==True 是真   1 is True 是假要内存id一致才真
            #print Res
            #print '\n',len(Res[u'userinfo'][u'mobile'])
            return Res[u'userinfo'][u'mobile']


      def change_phone(self,uid,old_mobile,pwd,new_phone):
            '''
            手机号码更换接口 ——需要发送验证码
            method get
            parameters  :userid  ,check_code ,old_mobile,pwd,mobile
            '''
            change_phone_url = urlparse.urljoin(URL,'mapi_v1/mobile/change')
            verify = self.send_verification(int(old_mobile),uid)
            if verify==False:
                  return False
            #verify= int(verify)
            #print type(verify)
            params = {
            'userid':uid,
            'check_code':verify,
            'old_moblie':old_mobile,
            'pwd':pwd,
            'mobile':new_phone
            }

            Res = requests.get(change_phone_url,params=params).json()
            #print Res

            if Res['status'] is 1:
                  return True
            else:
                  return False



      def collect_course_show(self,uid,begin_id,num):
            '''
            收藏接口
            fiter the collected courses at current user by user id,and show the courses from 'begin_id' to 'begin_id + num'
            method get
            parameters is 3,contain : 'userid'  'start'  and 'offset'
            '''

            coll_url = urlparse.urljoin(URL,r'mapi_v1/user/stow')
            data = {
            'userid':uid,
            'start':begin_id,
            'offset': num
            }
            data = urllib.urlencode(data)
            coll_url = coll_url+'?'+data
            res = urllib2.urlopen(coll_url).read()
            Res = json.loads(res)
            if Res['status'] is 0:
                  return False
            elif Res['status'] is 1:
                  cour_list =[]
                  for i in Res[u'courses']:
                        cour_list.append(i[u'title'])
                  return cour_list
            '''

            fname =os.path.join(os.path.dirname(__file__),'collect_courses.txt')
            f = file(fname,'a+')
            for i in Res[u'courses']:
                  f.write(i[u'id']+' : ')
                  f.write(i['title'].encode('utf-8')+'\n')
            f.write('-----------------------\n')
            f.close()
            return True
            '''

      def collect_add(self,uid,cid):
            '''
            添加收藏
            add courses in collection via userid and courseid
            method get
            parameter are 'userid' and 'courseid'
            '''
            coll_add_url =urlparse.urljoin(URL,r'mapi_v1/course/stow/add')
            data = {
            'userid': uid,
            'courseid' : cid
            }
            data = urllib.urlencode(data)
            coll_add_url = coll_add_url+'?'+data
            res = urllib2.urlopen(coll_add_url).read()
            Res = json.loads(res)

            if Res['status'] is 1:
                  return True
            else:
                  return False

      def collect_del(self,uid,cid):
            '''
            取消收藏
            remove courses in collection via userid and courseid,too
            method get
            parameter are 'userid' and 'courseid'
            '''
            coll_add_url =urlparse.urljoin(URL,r'mapi_v1/course/stow/remove')
            data = {
            'userid': uid,
            'courseid' : cid
            }
            data = urllib.urlencode(data)
            coll_add_url = coll_add_url+'?'+data
            res = urllib2.urlopen(coll_add_url).read()
            Res = json.loads(res)

            if Res['status'] is 1:
                  return True
            else:
                  return False

      def order_info(self,order_id):
            '''
            订单详情接口
            get the detail of order via id
            method get
            parameter is 'ordersn'
            '''
            order_infor_url = urlparse.urljoin(URL,r'mapi_v1/order/detail')
            data = {
            'ordersn' : order_id
            }
            data = urllib.urlencode(data)
            order_infor_url = order_infor_url+'?'+data
            res = urllib2.urlopen(order_infor_url).read()
            Res = json.loads(res)
            #print Res
            if Res['status'] is 0:
                  return False
            elif Res['status'] is 1:
                  ot = Res[u'order_info'][u'title']
                  #print ot.encode('utf-8')
                  return ot
            '''
            filename = os.path.join(os.path.dirname(__file__),'order_detail.txt')
            f = file(filename,'a+')
            i = Res[u'order_info']
            f.write('orderID' +': '+order_id+'\n')
            f.write('status' +': '+i['status']+'\n')
            f.write('title' +': '+i['title'].encode('utf-8')+'\n')
            f.write('\n')
            '''


      def order_list(self,uid,begining,num):
            '''
            个人订单列表获取接口
            show order list of someone 
            method get
            userid  start  offset

            '''
            order_list_url = urlparse.urljoin(URL,'mapi_v1/user/order/list')
            data = {
            'userid':uid,
            'begining':begining,
            'offset':num
            }
            data = urllib.urlencode(data)
            order_list_url = order_list_url+'?'+data
            res=urllib2.urlopen(order_list_url).read()
            Res = json.loads(res)
            #print Res
            if Res['status'] is 0:
                  return False
            else:
                  return True

      def head_portrait_change(self,uid,filename,image_stream):
            '''
            上传头像
            method post
            parameters are userid   myfile   imgstr
            上传头像图片涉及到将文件转换为二进制流文件，所以使用第三方库requests模块实现
            '''
            head_change_url = urlparse.urljoin(URL,'mapi_v1/user/avatar/upload')
            #print head_change_url
            data = {
            'userid':uid,
            'imgstr':image_stream
            }
            files = {
            'myfile':('111.jpg',open(filename,'rb'))
            }

            #将图片和接口参数 作文post方法的参数传递，并使用requests自动将返回的json信息转换为python可读
            Res = requests.post(head_change_url,data = data,files=files).json()
            #print Res
            if Res['status'] is 0:
                  return False
            else:
                  return True

      def modify_password(self,mobile,old_pwd,new_pwd):
            '''
            修改密码接口
            change the password
            method get
            parameters are mobile,old_pwd,new_pwd
            '''
            m_pwd_url = urlparse.urljoin(URL,'mapi_v1/user/password/reset')
            params = {
            'mobile' :mobile,
            'old_pwd':old_pwd,
            'new_pwd' :new_pwd
            }

            Res = requests.get(m_pwd_url,params=params).json()
            #print Res
            if Res['status'] =='0':
                  return False
            elif Res['status'] ==1:
                  return True

      def find_back_password(self,mobile,uid,new_pwd):
            '''
            找回密码接口
            找回密码 需要用mobile 和uid收到验证码，在执行找回密码
            method get
            params : mobile,check_code,password
            '''
            find_back_url = urlparse.urljoin(URL,r'mapi_v1/user/password/getback')
            verify = self.send_verification(int(mobile),uid)

            params = {
            'mobile':mobile,
            'check_code':verify,
            'password':new_pwd
            }

            Res = requests.get(find_back_url,params=params).json()
            #print Res
            if Res['status'] ==1:
                  return True
            else:
                  return False



      def modiy_user_info(self,uid,bd,sex):
            '''
            修改用户其他详细信息接口
            modiy the user information
            paramers userid ,birthday(xxxx-xx-xx),  gender(male female secret)
            '''

            modiy_url = urlparse.urljoin(URL,r'mapi_v1/user/profile/update')
            params = {
            'userid':uid,
            'birthday':bd,
            'gender':sex
            }
            Res = requests.get(modiy_url,params = params).json()
            print Res
            if Res['status'] is 0:
                  return False
            if Res['status'] is 1:
                  return True



      def send_verification(self,mobile,uid):
            '''
            发送手机验证码接口
            修改手机绑定、设置密码需要使用验证码
            method get
            parameters  mobile and userid
            '''
            try:
                  mobile = eval(mobile)
            except TypeError,e:
                  pass

            ver_url = urlparse.urljoin(URL,'mapi_v1/msg/send')
            params= {
            'mobile':mobile,
            'userid':uid
            }

            Res = requests.get(ver_url,params=params).json()
            #print Res
            
            if Res['status'] is 0:
                  return False
            if Res['status'] is 1:
                  con = MySQLdb.connect(host = '119.90.35.172',user = 'akkuser',passwd = '1234',port =3306,db ='akk_edu',charset='utf8')
                 #time.sleep(10) #可能出现数据库值为改变的情况等待一段时间，短信中心平台的机制不太清楚
                  cursor = con.cursor()
                  sql = "select check_code from user_profile where mobile='%s'"
                  cursor.execute(sql,(mobile))
                  check_code = cursor.fetchall()
                  if len(check_code)==0:
                        return False
                  Code =check_code[0][0]
                  cursor.close()
                  con.close()
                  #print Code
                  return Code
            

            #'select * from user_profile where mobile="18511567123"'

      def get_all_course(self):
            '''获取所有类目接口
            method get
            no parameters
            '''
            all_class_url = urlparse.urljoin(URL,r'mapi_v1/course/category')
            Res = requests.get(all_class_url).json()
            #print Res
            #需要显示课程 没有status
            class_list =[]
            for i in Res:
                  class_list.append(i[ u'name'])
            return class_list

      def get_all_course_by_fclass(self):
            '''
            获取所有类目，且每个类目带4个课程接口
            method get
            no parameters
            '''
            all_class_url = urlparse.urljoin(URL,r'mapi_v1/course/get/all')
            Res = requests.get(all_class_url).json()
            print Res
            classes ={}
            for i in Res:
                  classes[i[u'name']] =[]
                  for j in i[u'courses']:
                        classes[i[u'name']].append(j[u'title'])
            return classes





      def get_course_by_page_num(self,pages,num):
            '''
            获取所有课程接口
            设置每页显示num条课程，显示第pages页的课程
            method post
            params :page, limit
            '''
            get_courses_url = urlparse.urljoin(URL,'mapi_v1/all/course')
            data = {
            'page':pages,
            'limit':num
            }

            Res = requests.post(get_courses_url,data= data).json()
            #print Res
            if Res['status'] is 0:
                  return False
            if Res['status'] is 1:
                  return True


      def get_class_of_all_courses(self,clas,pages):
            '''
            获取某个类目的所有课程接口
            method post
            params  are 'category_id' ,'page' 
            '''
            get_ccourses_url = urlparse.urljoin(URL,'mapi_v1/get/category/course')
            #print get_ccourses_url
            data = {
            'category_id':clas,
            'page':pages
            }
            Res = requests.post(get_ccourses_url,data=data).json()
            c_list=[]
            for i in Res:
                  c_list.append(i[u'title'])
            return c_list
            
      def get_info_course(self,courseid,uid):
            '''获取课程详情
            method post
            params are  course_id,userid
            '''
            get_info_url = urlparse.urljoin(URL,'mapi_v1/get/course/details')
            data = {
            'course_id':courseid,
            'userid':uid
            }
            Res = requests.post(get_info_url,data=data).json()
            #print Res
            if  '0'  not in Res.keys():
                  return False
            elif Res[u'0'][u'title']:
                  #print Res[u'0'][u'title'].encode('utf-8')
                  return Res[u'0'][u'title'].encode('utf-8')

            
                  


      def get_class_list(self,courseid,page):
            '''
            获取课程的课时列表
            post
            course_id,page
            '''
            get_all_class_url = urlparse.urljoin(URL,'mapi_v1/get/course/lesson/list')
            data = {
            'course_id':courseid,
            'page':page
            }
            Res = requests.post(get_all_class_url,data=data).json()
            #print Res
            class_list =[]
            if len(Res) is 0:
                  return False
            else:
                  for i in Res:
                        class_list.append(i[u'lessons_title'])
                  return class_list
                  


      def get_comment(self,courseid,page):
            '''
            获取课程的评价列表
            post
            course_id,page
            '''
            get_comment_url = urlparse.urljoin(URL,'mapi_v1/courses/reviews/list')
            data = {
            'course_id':courseid,
            'page':page
            }
            Res =requests.post(get_comment_url,data).json()
            comm = []
            #print Res
            if Res[u'total'] =='0':
                  return False
            else:
                  for i in Res[u'data']:
                        comm.append(i[ u'content'])
                  return comm

      def add_comment(self,uid,courseid,content,grade):
            '''
            添加评论
            post
            params are userid,course_id,content,rating,
            '''
            add_comment_url = urlparse.urljoin(URL,'mapi_v1/user/courses/reviews/create')
            data = {
            'userid':uid,
            'course_id':courseid,
            'content':content,
            'rating':grade
            }
            Res = requests.post(add_comment_url,data = data).json()
            #print Res
            if len(Res)==2:
                  return False
            else:
                  return Res[u'content']

      def add_course(self,courseid,uid):
            '''
            添加学习课程
            post
            course_id, userid
            '''
            add_course_url = urlparse.urljoin(URL,'mapi_v1/user/courses/pay')
            data = {
            'course_id':courseid,
            'userid':uid
            }
            Res = requests.post(add_course_url,data=data).json()
            #print Res
            if Res[u'status'] == '0':
                  return False
            else:
                  return True


      def get_courses_list(self,uid,num,page):
            ''' 个人课程列表（付费、免费）根据当前登录者获取
            get
            userid, limit, page 
            '''
            get_courses_url = urlparse.urljoin(URL,'mapi_v1/user/all/course')
            params = {
            'userid':uid,
            'limit':num,
            'page':page
            }
            Res =requests.get(get_courses_url,params=params).json()
            #print Res
            coures = []
            if len(Res) ==2:
                  return False
            else:
                  for i in Res[u'data']:
                        coures.append(i[u'title'])
                  return coures

            

      def filter_list(self,cotent,page):
            '''搜索
            post
            title, page
            '''
            filter_url = urlparse.urljoin(URL,'mapi_v1/get/category/course')
            data = {
            'title':cotent,
            'page':page
            }
            Res = requests.post(filter_url,data=data).json()
            #print Res
            cour_titles = []
            if len(Res) is 0:
                  return False
            else:
                  for i in Res:
                        cour_titles.append(i[u'title'])
                  return cour_titles


      def get_collect_cousrses_order(self,uid):
            '''获取收藏数、课程数、订单数
            get
            userid
            '''
            get_collection_url = urlparse.urljoin(URL,'mapi_v1/get/stowcourseorder/info')
            params = {
            'userid':uid
            }
            Res = requests.get(get_collection_url,params=params).json()
            #print Res
            return Res # 注意返回的数字是字符格式

      def  payment(self):
            '''支付
            '''
            pass


