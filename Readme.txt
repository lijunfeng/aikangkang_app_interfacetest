
--------------第一版---------------------------------------------------------
aikangkang_app_interface_test.py 为基类
case.py                          为测试用例

一环境 
import urllib,urllib2,urlparse,requests
import json,os,time,re
import MySQLdb
其中 requests he MySQLdb 需要安装


本次接口测试未考虑接口的元素格式（接口格式和元素类型由后台开发给出，未经过前端和测试审核）——弊端是固定规则，如：‘status’返回1 或者‘1’

二 测试code优化
1 部分用例可以和数据库最对比
2 可以写一个log文件，将测试数据和测试用例内容及详细测试结果写入
以上两项需要大量劳力

三 本项目问题：
接口给出是有后台开发提供，没有经过审核。没有固定的规则，如：一些接口的‘status’返回1 有些却返回‘1’
